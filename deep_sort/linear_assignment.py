# vim: expandtab:ts=4:sw=4
from __future__ import absolute_import

import math
import numpy as np
from scipy.optimize import linear_sum_assignment as linear_assignment
from . import kalman_filter


INFTY_COST = 1e+5


def get_size_gating_matrix(size_threshold, tracks, detections, track_indices, detection_indices):
    tracks = [tracks[i] for i in track_indices]
    cur_detections = [detections[i] for i in detection_indices]
    cost_matrix = np.zeros((len(track_indices), len(detection_indices)), dtype=np.float32)

    for i, track in enumerate(tracks):
        track_width, track_height = track.to_tlwh()[2:]
        track_size = track_width * track_height

        #time_factor = 2 * math.log(track.time_since_update) + 1
        time_factor = track.time_since_update
        #min_size_threshold = (1 - size_threshold) / time_factor
        #max_size_threshold = (1 + size_threshold) * time_factor
        min_size_threshold = 1 - size_threshold * time_factor
        max_size_threshold = 1 + size_threshold * time_factor

        min_feasible_size = track_size * min_size_threshold
        max_feasible_size = track_size * max_size_threshold
        for j, detection in enumerate(cur_detections):
            det_width, det_height = detection.tlwh[2:]
            det_size = det_width * det_height
            if det_size >= max_feasible_size or det_size <= min_feasible_size:
                cost_matrix[i][j] = 1e12
    return cost_matrix


def get_cost_matrix(distance_metric, max_distance, penalty, tracks, detections, track_indices, detection_indices):
    cost_matrix = distance_metric(tracks, detections, track_indices, detection_indices)
    cost_matrix[cost_matrix > max_distance] = penalty + 1e-5
    return cost_matrix


def min_cost_matching_combined(metric_1, metric_2, max_dist_1, max_dist_2, tracks, detections, track_indices=None,
                               detection_indices=None, debugger=None, size_threshold=None, reid_weight=1.0, tentative=False,
                               tolerance=0.4):
    if track_indices is None:
        track_indices = np.arange(len(tracks))
    if detection_indices is None:
        detection_indices = np.arange(len(detections))

    if len(detection_indices) == 0 or len(track_indices) == 0:
        return [], track_indices, detection_indices  # Nothing to match.

    #penalty_1 = max_dist_1 + 0.0001 
    #penalty_2 = max_dist_2 + 0.1

    #cost_matrix_1 = get_cost_matrix(metric_1, max_dist_1, penalty_1, tracks, detections, track_indices, detection_indices)
    #cost_matrix_2 = get_cost_matrix(metric_2, max_dist_2, penalty_2, tracks, detections, track_indices, detection_indices)

    penalty = 2. - tolerance

    min_dist_1 = 0.0010
    min_dist_2 = 0.

    cost_matrix_1 = metric_1(tracks, detections, track_indices, detection_indices)
    cost_matrix_2 = metric_2(tracks, detections, track_indices, detection_indices)

    cost_matrix_1 = (cost_matrix_1 - min_dist_1) / (max_dist_1 - min_dist_1)
    cost_matrix_2 = (cost_matrix_2 - min_dist_2) / (max_dist_2 - min_dist_2)

    cost_matrix_1[cost_matrix_1 > 1.0] = penalty
    cost_matrix_2[cost_matrix_2 > 1.0] = penalty

    cur_tracks = np.array([tracks[i] for i in track_indices])

    factor = 0.1
    iou_weights = np.array([math.exp(-t.time_since_update * factor + factor) for t in cur_tracks])
    iou_weights[iou_weights < 1e-6] = 0.0
    iou_weights = np.expand_dims(iou_weights, axis=1)

    weight_total = iou_weights + reid_weight
    cost_matrix = reid_weight * cost_matrix_1 + iou_weights * cost_matrix_2
    cost_matrix /= weight_total

    #max_distances = (iou_weights + max_dist_1 * reid_weight) / weight_total
    #max_distances = (iou_weights + reid_weight) / weight_total
    max_dist = 1.

    if size_threshold is not None:
        size_cost_matrix = get_size_gating_matrix(size_threshold, tracks, detections, track_indices, detection_indices)
        cost_matrix += size_cost_matrix
    
    if debugger:
        detection_ids = [detections[idx].id for idx in detection_indices]
        types = ['appearance', 'iou', 'combined', 'size']
        if tentative:
            types = [f'tentative_{t}' for t in types]
        debugger.save_cost_matrix(cost_matrix_1, track_indices, detection_ids, type_=types[0])
        debugger.save_cost_matrix(cost_matrix_2, track_indices, detection_ids, type_=types[1])
        debugger.save_cost_matrix(cost_matrix, track_indices, detection_ids, type_=types[2])
        if size_threshold is not None:
            debugger.save_cost_matrix(size_cost_matrix, track_indices, detection_ids, type_=types[3])


    indices = linear_assignment(cost_matrix)
    indices = np.array([[ind_1, ind_2] for ind_1, ind_2 in zip(indices[0], indices[1])])

    matches, unmatched_tracks, unmatched_detections = [], [], []
    for col, detection_idx in enumerate(detection_indices):
        if col not in indices[:, 1]:
            unmatched_detections.append(detection_idx)
    for row, track_idx in enumerate(track_indices):
        if row not in indices[:, 0]:
            unmatched_tracks.append(track_idx)
    for row, col in indices:
        track_idx = track_indices[row]
        detection_idx = detection_indices[col]
        #if cost_matrix[row, col] > max_distances[row][0]:
        if cost_matrix[row, col] >= max_dist:
            unmatched_tracks.append(track_idx)
            unmatched_detections.append(detection_idx)
        else:
            matches.append((track_idx, detection_idx))
    return matches, unmatched_tracks, unmatched_detections


def min_cost_matching(distance_metric, max_distance, tracks, detections, track_indices=None,
                      detection_indices=None, debugger=None, size_threshold=None):
    """Solve linear assignment problem.

    Parameters
    ----------
    distance_metric : Callable[List[Track], List[Detection], List[int], List[int]) -> ndarray
        The distance metric is given a list of tracks and detections as well as
        a list of N track indices and M detection indices. The metric should
        return the NxM dimensional cost matrix, where element (i, j) is the
        association cost between the i-th track in the given track indices and
        the j-th detection in the given detection_indices.
    max_distance : float
        Gating threshold. Associations with cost larger than this value are
        disregarded.
    tracks : List[track.Track]
        A list of predicted tracks at the current time step.
    detections : List[detection.Detection]
        A list of detections at the current time step.
    track_indices : List[int]
        List of track indices that maps rows in `cost_matrix` to tracks in
        `tracks` (see description above).
    detection_indices : List[int]
        List of detection indices that maps columns in `cost_matrix` to
        detections in `detections` (see description above).
    debugger (DeepSortDebugger): Class that saves tracking information for debug.

    Returns
    -------
    (List[(int, int)], List[int], List[int])
        Returns a tuple with the following three entries:
        * A list of matched track and detection indices.
        * A list of unmatched track indices.
        * A list of unmatched detection indices.

    """
    if track_indices is None:
        track_indices = np.arange(len(tracks))
    if detection_indices is None:
        detection_indices = np.arange(len(detections))

    if len(detection_indices) == 0 or len(track_indices) == 0:
        return [], track_indices, detection_indices  # Nothing to match.

    cost_matrix = get_cost_matrix(distance_metric, max_distance, max_distance, tracks, detections, track_indices, detection_indices)
    if size_threshold is not None:
        size_cost_matrix = get_size_gating_matrix(size_threshold, tracks, detections, track_indices, detection_indices)
        cost_matrix += size_cost_matrix

    if debugger:
        detection_ids = [detections[idx].id for idx in detection_indices]
        type_ = 'iou' if distance_metric.__name__ == 'iou_cost' else 'appearance'
        debugger.save_cost_matrix(cost_matrix, track_indices, detection_ids, type_=type_)

    indices = linear_assignment(cost_matrix)
    indices = np.array([[ind_1, ind_2] for ind_1, ind_2 in zip(indices[0], indices[1])])

    matches, unmatched_tracks, unmatched_detections = [], [], []
    for col, detection_idx in enumerate(detection_indices):
        if col not in indices[:, 1]:
            unmatched_detections.append(detection_idx)
    for row, track_idx in enumerate(track_indices):
        if row not in indices[:, 0]:
            unmatched_tracks.append(track_idx)
    for row, col in indices:
        track_idx = track_indices[row]
        detection_idx = detection_indices[col]
        if cost_matrix[row, col] > max_distance:
            unmatched_tracks.append(track_idx)
            unmatched_detections.append(detection_idx)
        else:
            matches.append((track_idx, detection_idx))
    return matches, unmatched_tracks, unmatched_detections


def direct_matching(distance_metric, max_distance, tracks, detections,
                    track_indices=None, detection_indices=None, debugger=None):
    """Runs direct matching

    Parameters
    ----------
    distance_metric : Callable[List[Track], List[Detection], List[int], List[int]) -> ndarray
        The distance metric is given a list of tracks and detections as well as
        a list of N track indices and M detection indices. The metric should
        return the NxM dimensional cost matrix, where element (i, j) is the
        association cost between the i-th track in the given track indices and
        the j-th detection in the given detection indices.
    max_distance : float
        Gating threshold. Associations with cost larger than this value are
        disregarded.
    cascade_depth: int
        The cascade depth, should be se to the maximum track age.
    tracks : List[track.Track]
        A list of predicted tracks at the current time step.
    detections : List[detection.Detection]
        A list of detections at the current time step.
    track_indices : Optional[List[int]]
        List of track indices that maps rows in `cost_matrix` to tracks in
        `tracks` (see description above). Defaults to all tracks.
    detection_indices : Optional[List[int]]
        List of detection indices that maps columns in `cost_matrix` to
        detections in `detections` (see description above). Defaults to all
        detections.
    debugger (DeepSortDebugger): Class that saves tracking information for debug.

    Returns
    -------
    (List[(int, int)], List[int], List[int])
        Returns a tuple with the following three entries:
        * A list of matched track and detection indices.
        * A list of unmatched track indices.
        * A list of unmatched detection indices.
    """
    if track_indices is None:
        track_indices = list(range(len(tracks)))
    if detection_indices is None:
        detection_indices = list(range(len(detections)))

    if len(detection_indices) == 0 or len(track_indices) == 0:
        return [], track_indices, detection_indices

    matches, _, unmatched_detections = \
        min_cost_matching(
            distance_metric, max_distance, tracks, detections,
            track_indices, detection_indices, debugger)

    unmatched_tracks = list(set(track_indices) - set(k for k, _ in matches))
    return matches, unmatched_tracks, unmatched_detections



def matching_cascade(distance_metric, max_distance, cascade_depth, tracks, detections,
                     track_indices=None, detection_indices=None, debugger=None):
    """Runs matching cascade.

    Parameters
    ----------
    distance_metric : Callable[List[Track], List[Detection], List[int], List[int]) -> ndarray
        The distance metric is given a list of tracks and detections as well as
        a list of N track indices and M detection indices. The metric should
        return the NxM dimensional cost matrix, where element (i, j) is the
        association cost between the i-th track in the given track indices and
        the j-th detection in the given detection indices.
    max_distance : float
        Gating threshold. Associations with cost larger than this value are
        disregarded.
    cascade_depth: int
        The cascade depth, should be se to the maximum track age.
    tracks : List[track.Track]
        A list of predicted tracks at the current time step.
    detections : List[detection.Detection]
        A list of detections at the current time step.
    track_indices : Optional[List[int]]
        List of track indices that maps rows in `cost_matrix` to tracks in
        `tracks` (see description above). Defaults to all tracks.
    detection_indices : Optional[List[int]]
        List of detection indices that maps columns in `cost_matrix` to
        detections in `detections` (see description above). Defaults to all
        detections.
    debugger (DeepSortDebugger): Class that saves tracking information for debug.

    Returns
    -------
    (List[(int, int)], List[int], List[int])
        Returns a tuple with the following three entries:
        * A list of matched track and detection indices.
        * A list of unmatched track indices.
        * A list of unmatched detection indices.
    """
    if track_indices is None:
        track_indices = list(range(len(tracks)))
    if detection_indices is None:
        detection_indices = list(range(len(detections)))

    unmatched_detections = detection_indices
    matches = []
    depths = set()
    for k in track_indices:
        depths.add(tracks[k].time_since_update)

    depths = list(depths)
    depths.sort()

    for level in depths:
        if len(unmatched_detections) == 0:  # No detections left
            break

        track_indices_l = [
            k for k in track_indices
            if tracks[k].time_since_update == level
        ]
        if len(track_indices_l) == 0:  # Nothing to match at this level
            continue

        matches_l, _, unmatched_detections = \
            min_cost_matching(
                distance_metric, max_distance, tracks, detections,
                track_indices_l, unmatched_detections, debugger)
        matches += matches_l
    unmatched_tracks = list(set(track_indices) - set(k for k, _ in matches))
    return matches, unmatched_tracks, unmatched_detections


def gate_cost_matrix(
        kf, cost_matrix, tracks, detections, track_indices, detection_indices,
        gated_cost=INFTY_COST, only_position=False):
    """Invalidate infeasible entries in cost matrix based on the state
    distributions obtained by Kalman filtering.

    Parameters
    ----------
    kf : The Kalman filter.
    cost_matrix : ndarray
        The NxM dimensional cost matrix, where N is the number of track indices
        and M is the number of detection indices, such that entry (i, j) is the
        association cost between `tracks[track_indices[i]]` and
        `detections[detection_indices[j]]`.
    tracks : List[track.Track]
        A list of predicted tracks at the current time step.
    detections : List[detection.Detection]
        A list of detections at the current time step.
    track_indices : List[int]
        List of track indices that maps rows in `cost_matrix` to tracks in
        `tracks` (see description above).
    detection_indices : List[int]
        List of detection indices that maps columns in `cost_matrix` to
        detections in `detections` (see description above).
    gated_cost : Optional[float]
        Entries in the cost matrix corresponding to infeasible associations are
        set this value. Defaults to a very large value.
    only_position : Optional[bool]
        If True, only the x, y position of the state distribution is considered
        during gating. Defaults to False.

    Returns
    -------
    ndarray
        Returns the modified cost matrix.

    """
    gating_dim = 2 if only_position else 4
    gating_threshold = kalman_filter.chi2inv95[gating_dim]
    measurements = np.asarray(
        [detections[i].to_xyah() for i in detection_indices])
    for row, track_idx in enumerate(track_indices):
        track = tracks[track_idx]
        gating_distance = kf.gating_distance(
            track.mean, track.covariance, measurements, only_position)
        cost_matrix[row, gating_distance > gating_threshold] = gated_cost
    return cost_matrix
