import numpy as np


class TrackInfo:
    """Class that hold tracking information"""
    def __init__(self, track_id, detection_id=None, feature=None, state=None,
                 frame_index=None, merge_to=None, merge_from=None):
        """
        Args:
            track_id (int): The id of the track.
            feature (nd.array): Appearance feature of the track.
            state (TrackState): State of the track, please refer to the TrackState class.
        """
        self.track_id = track_id
        self.feature = feature
        self.state = state
        self.detection_id = detection_id
        self.merge_to = merge_to
        self.merge_from = merge_from
        if frame_index:
            self.frame_index = int(frame_index)
        else:
            self.frame_index = None


class TrackState:
    """
    Enumeration type for the single target track state. Newly created tracks are
    classified as `tentative` until enough evidence has been collected. Then,
    the track state is changed to `confirmed`. Tracks that are no longer alive
    are classified as `deleted` to mark them for removal from the set of active
    tracks.

    """
    Tentative = 1
    Confirmed = 2
    Deleted = 3
    Missed = 4
    Merging = 5


class Track:
    """
    A single target track with state space `(x, y, a, h)` and associated
    velocities, where `(x, y)` is the center of the bounding box, `a` is the
    aspect ratio and `h` is the height.

    Parameters
    ----------
    mean : ndarray
        Mean vector of the initial state distribution.
    covariance : ndarray
        Covariance matrix of the initial state distribution.
    track_id : int
        A unique track identifier.
    n_init : int
        Number of consecutive detections before the track is confirmed. The
        track state is set to `Deleted` if a miss occurs within the first
        `n_init` frames.
    max_age : int
        The maximum number of consecutive misses before the track state is
        set to `Deleted`.
    feature : Optional[ndarray]
        Feature vector of the detection this track originates from. If not None,
        this feature is added to the `features` cache.

    Attributes
    ----------
    mean : ndarray
        Mean vector of the initial state distribution.
    covariance : ndarray
        Covariance matrix of the initial state distribution.
    track_id : int
        A unique track identifier.
    hits : int
        Total number of measurement updates.
    age : int
        Total number of frames since first occurance.
    time_since_update : int
        Total number of frames since last measurement update.
    state : TrackState
        The current track state.
    features : List[ndarray]
        A cache of features. On each measurement update, the associated feature
        vector is added to this list.

    """

    def __init__(self, mean, covariance, track_id, n_init, max_age,
                 detection, start_frame, feature=None):
        self.mean = mean
        self.covariance = covariance
        self.track_id = track_id
        self.hits = 1
        self.age = 1
        self.time_since_update = 0

        self.features = None
        if feature is not None:
            self.features = np.expand_dims(feature, axis=0)

        self.frame_index = start_frame
        self.detections = {str(start_frame) : detection}
        self._n_init = n_init
        self._max_age = max_age
        self.state = TrackState.Tentative


    def merge(self, other, budget=300):
        """Merges the track with another one

        Args:
            other (Track): The other track to merge. 
            budget (int): Maximum number of features to keep for the track.
        """
        self.mean = other.mean
        self.covariance = other.covariance
        self.hits += other.hits
        self.age = 1
        self.time_since_update = 0
        self.state = TrackState.Confirmed
        self.features = np.concatenate([self.features, other.features], axis=0)

        assert list(self.detections.keys()) not in list(other.detections.keys()), 'tracks overlap.'
        self.detections.update(other.detections)
        self.frame_index = other.frame_index

        nb_features = len(self.features)
        if self.hits > budget:
            self.hits = budget
        if nb_features > budget:
            self.features = np.random.permutation(self.features)[:budget, :]
            

    def to_tlwh(self):
        """Get current position in bounding box format `(top left x, top left y,
        width, height)`.

        Returns
        -------
        ndarray
            The bounding box.

        """
        ret = self.mean[:4].copy()
        ret[2] *= ret[3]
        ret[:2] -= ret[2:] / 2
        return ret

    def to_tlbr(self):
        """Get current position in bounding box format `(min x, miny, max x,
        max y)`.

        Returns
        -------
        ndarray
            The bounding box.

        """
        ret = self.to_tlwh()
        ret[2:] = ret[:2] + ret[2:]
        return ret

    def predict(self, kf):
        """Propagate the state distribution to the current time step using a
        Kalman filter prediction step.

        Parameters
        ----------
        kf : kalman_filter.KalmanFilter
            The Kalman filter.

        """
        self.age += 1
        self.time_since_update += 1

    def update(self, kf, detection, frame_index, budget=10, debugger=None, temporary_metric=False,
               add_to_gallery=True):
        """Perform Kalman filter measurement update step and update the feature
        cache.

        Parameters
        ----------
        kf : kalman_filter.KalmanFilter
            The Kalman filter.
        detection : Detection
            The associated detection.

        """
        assert self.frame_index < frame_index, 'cannot go into the past.'
        self.frame_index = frame_index

        self.mean, self.covariance = kf.initiate(detection.to_xyah())
        new_feature = detection.feature
        if add_to_gallery and len(self.features) < budget:
            self.features = np.concatenate([self.features, np.expand_dims(new_feature, axis=0)], axis=0)
        self.detections[str(frame_index)] = detection

        self.hits += 1
        self.time_since_update = 0

        if self.hits >= self._n_init:
            if self.state == TrackState.Tentative:
                if not temporary_metric:
                # TODO: merge tentative track with the confirmed gallery
                    self.state = TrackState.Confirmed
                else:
                    self.state = TrackState.Merging
            elif self.state == TrackState.Missed:
                self.state = TrackState.Confirmed

        if debugger:
            debugger.save_track_info([TrackInfo(self.track_id, detection.id, new_feature, self.state)],
                                     add_to_gallery=add_to_gallery)
        return self.state == TrackState.Merging

    def delete(self):
        """Sets the track as deleted"""
        self.state = TrackState.Deleted

    def mark_missed(self, debugger=None):
        """Mark this track as missed (no association at the current time step).
        """
        old_state = self.state
        if self.state == TrackState.Tentative and self.time_since_update > self._max_age:
            self.state = TrackState.Deleted 
        elif self.state == TrackState.Confirmed and self.time_since_update > 5:
        #elif self.state == TrackState.Confirmed:
            self.state = TrackState.Missed
        if debugger and self.state != old_state:
            debugger.save_track_info([TrackInfo(self.track_id, state=self.state, frame_index=self.frame_index)])

    def is_tentative(self):
        """Returns True if this track is tentative (unconfirmed).
        """
        return self.state == TrackState.Tentative

    def is_confirmed(self):
        """Returns True if this track is confirmed."""
        return self.state == TrackState.Confirmed or self.state == TrackState.Missed

    def is_active(self):
        """Returns True if this track is confirmed and active"""
        return self.state == TrackState.Confirmed

    def is_deleted(self):
        """Returns True if this track is dead and should be deleted."""
        return self.state == TrackState.Deleted

    def is_missed(self):
        """Returns True if this track got confirmed and was missed"""
        return self.state == TrackState.Missed
