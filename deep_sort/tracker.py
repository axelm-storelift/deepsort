from __future__ import absolute_import
import numpy as np
from . import kalman_filter
from . import linear_assignment
from . import iou_matching
from .track import Track
from .track import TrackInfo
from .track import TrackState
from .nn_matching import _cosine_distance


class Tracker:
    """
    This is the multi-target tracker.

    Parameters
    ----------
    metric : nn_matching.NearestNeighborDistanceMetric
        A distance metric for measurement-to-track association.
    max_age : int
        Maximum number of missed misses before a track is deleted.
    n_init : int
        Number of consecutive detections before the track is confirmed. The
        track state is set to `Deleted` if a miss occurs within the first
        `n_init` frames.

    Attributes
    ----------
    metric : nn_matching.NearestNeighborDistanceMetric
        The distance metric used for measurement to track association.
    max_age : int
        Maximum number of missed misses before a track is deleted.
    n_init : int
        Number of frames that a track remains in initialization phase.
    kf : kalman_filter.KalmanFilter
        A Kalman filter to filter target trajectories in image space.
    tracks : List[Track]
        The list of active tracks at the current time step.
    debugger : TrackingInfoSaver
        A class that logs the tracking informations at each frame.
    cascade: bool
        Whether to use cascade or direct matching.

    """

    def __init__(self, metric, max_iou_distance=0.7, max_age=30, n_init=3, budget=5000, debugger=None,
                 cascade=True, size_threshold=None, reid_weight=1.0, reid_threshold=0.0022, temporary_metric=None,
                 tolerance=0.4, rerank=False, gallery_similarity_threshold=None):
        self.metric = metric
        self.max_iou_distance = max_iou_distance
        self.max_age = max_age
        self.n_init = n_init
        self.kf = kalman_filter.KalmanFilter()
        self.tracks = []
        self._next_id = 0
        self.budget = budget
        self.debugger = debugger
        self.cascade = cascade
        self.size_threshold = size_threshold
        self.reid_threshold = reid_threshold
        self.reid_weight = reid_weight
        self.temporary_metric = temporary_metric
        self.frame_index = -1
        self.tolerance = tolerance
        self.rerank = rerank
        self.gallery_similarity_threshold = gallery_similarity_threshold


    def reset(self):
        self.tracks = []
        self._next_id = 0
        self.metric.samples = {}
        self.frame_index = -1
        if self.temporary_metric:
            self.temporary_metric.samples = {}


    def predict(self):
        """Propagate track state distributions one time step forward.

        This function should be called once every time step, before `update`.
        """
        self.frame_index += 1
        for track in self.tracks:
            track.predict(self.kf)


    def update(self, detections):
        """Perform measurement update and track management.

        Parameters
        ----------
        detections : List[deep_sort.detection.Detection]
            A list of detections at the current time step.

        """
        # Run matching cascade.
        matches, unmatched_tracks, unmatched_detections = \
            self._match(detections)

        # Update track set.
        track_to_merge = []
        for track_idx, detection_idx in matches:
            add_to_gallery = True
            cur_det = detections[detection_idx]
            cur_track = self.tracks[track_idx]

            if self.gallery_similarity_threshold is not None:
                new_feature = np.expand_dims(cur_det.feature, axis=0)
                within_track_distances = _cosine_distance(new_feature, cur_track.features)

                min_dist_within_track = within_track_distances.min()
                if self.gallery_similarity_threshold < min_dist_within_track:
                    add_to_gallery = True
                else:
                    add_to_gallery = False

            if cur_track.update(
                self.kf, cur_det,
                self.frame_index,
                budget=self.budget,
                debugger=self.debugger,
                temporary_metric=self.temporary_metric,
                add_to_gallery=add_to_gallery
            ):
                track_to_merge.append(self.tracks[track_idx])
        for track_idx in unmatched_tracks:
            self.tracks[track_idx].mark_missed(debugger=self.debugger)
        for detection_idx in unmatched_detections:
            self._initiate_track(detections[detection_idx])
        self.tracks = [t for t in self.tracks if not t.is_deleted()]

        # Update distance metric.
        active_targets = [track for track in self.tracks if track.is_confirmed()]
        self.metric.samples = {track.track_id : track.features for track in active_targets}

        if self.temporary_metric:
            if len(track_to_merge) != 0:
                missed_tracks = [track for track in self.tracks if track.is_missed()]

            tracks_to_confirm = []
            for track in track_to_merge:
                if len(missed_tracks) == 0:
                    tracks_to_confirm.append(track)
                    continue

                valid_tracks = []
                for missed_track in missed_tracks:
                    missed_track_frame_indexes = list(missed_track.detections.keys())
                    if not any(frame_index for frame_index in  list(track.detections.keys()) \
                        if frame_index in missed_track_frame_indexes):
                        valid_tracks.append(missed_track.track_id)

                if len(valid_tracks) == 0: 
                    tracks_to_confirm.append(track)
                    continue

                dist = self.metric.distance(track.features, valid_tracks, rerank=self.rerank)
                dist_mean = np.mean(dist, axis=1)

                if self.debugger:
                    self.debugger.log_reid_info(np.expand_dims(dist_mean, axis=0), [track.track_id], valid_tracks, self.frame_index)

                valid_candidates = np.where(dist_mean < self.reid_threshold)[0]

                if len(valid_candidates) == 0: 
                    tracks_to_confirm.append(track)
                    continue
                    
                min_idx = np.argmin(dist_mean[valid_candidates])
                reassigned_id = valid_tracks[valid_candidates[min_idx]]
                reassigned_track = self.reassign(reassigned_id, track.track_id)
                tracks_to_confirm.append(reassigned_track)

            for track in tracks_to_confirm:
                track.state = TrackState.Confirmed
                self.metric.samples[track.track_id] = track.features

                if self.debugger:
                    self.debugger.save_track_info([TrackInfo(track.track_id, state=track.state, frame_index=self.frame_index)])

            tentative_targets = [track for track in self.tracks if track.is_tentative()]
            self.temporary_metric.samples = {track.track_id : track.features for track in tentative_targets}


    def reassign(self, old_id, new_id):
        """Merges two tracks, the new Track is merge into old one and deleted.

        Args:
            old_id: The id of the track that has disappeared.
            new_id: The id of the new track.
        """
        old_track = None
        new_track = None
        for t in self.tracks:
            if t.track_id == old_id:
                old_track = t 
            elif t.track_id == new_id:
                new_track = t
        assert old_track is not None and new_track is not None, \
            'cannot find old or new track.'

        old_track.merge(new_track, budget=self.budget)
        new_track.delete() 

        self.tracks = [t for t in self.tracks if not t.is_deleted()]
        active_targets = [track for track in self.tracks if track.is_confirmed()]
        self.metric.samples = {track.track_id : track.features for track in active_targets}

        if self.debugger:
            track_infos = []
            for frame_index in new_track.detections:
                detection = new_track.detections[frame_index]
                track_infos.append(TrackInfo(new_track.track_id, frame_index=frame_index, merge_to=old_track.track_id))

            for frame_index in old_track.detections:
                detection = old_track.detections[frame_index]
                track_infos.append(
                    TrackInfo(
                        old_track.track_id,
                        detection_id=detection.id,
                        feature=detection.feature,
                        state=1,
                        frame_index=frame_index,
                        merge_from=new_track.track_id
                    )
                )
            self.debugger.save_track_info(track_infos)

        return old_track


    def _match(self, detections):
        def gated_metric(tracks, dets, track_indices, detection_indices):
            features = np.array([dets[i].feature for i in detection_indices])
            targets = np.array([tracks[i].track_id for i in track_indices])
            cost_matrix = self.metric.distance(features, targets)
            return cost_matrix

        def gated_temporary_metric(tracks, dets, track_indices, detection_indices):
            features = np.array([dets[i].feature for i in detection_indices])
            targets = np.array([tracks[i].track_id for i in track_indices])
            cost_matrix = self.temporary_metric.distance(features, targets)
            return cost_matrix


        # Associate confirmed tracks using appearance features.
        if self.cascade:
            # Split track set into confirmed and unconfirmed tracks.
            confirmed_tracks = [
                i for i, t in enumerate(self.tracks) if t.is_confirmed()]
            unconfirmed_tracks = [
                i for i, t in enumerate(self.tracks) if not t.is_confirmed()]
            matches_a, unmatched_tracks_a, unmatched_detections = linear_assignment.matching_cascade(
                gated_metric, self.metric.matching_threshold, self.max_age, self.tracks,
                detections, confirmed_tracks, debugger=self.debugger
            )

            iou_track_candidates = unconfirmed_tracks + [
                k for k in unmatched_tracks_a if
                self.tracks[k].time_since_update == 1]
            unmatched_tracks_a = [
                k for k in unmatched_tracks_a if
                self.tracks[k].time_since_update != 1]
            matches_b, unmatched_tracks_b, unmatched_detections = \
                linear_assignment.min_cost_matching(
                    iou_matching.iou_cost, self.max_iou_distance, self.tracks,
                    detections, iou_track_candidates, unmatched_detections, debugger=self.debugger)
        else:
            if not self.temporary_metric:
                confirmed_tracks = [
                    i for i, t in enumerate(self.tracks) if t.is_confirmed()]
                unconfirmed_tracks = [
                    i for i, t in enumerate(self.tracks) if not t.is_confirmed()]
            else:
                confirmed_tracks = [
                    i for i, t in enumerate(self.tracks) if t.is_active()]
                unconfirmed_tracks = [
                    i for i, t in enumerate(self.tracks) if t.is_tentative()]

            matches_a, unmatched_tracks_a, unmatched_detections = linear_assignment.min_cost_matching_combined(
                gated_metric, iou_matching.iou_cost, self.metric.matching_threshold, self.max_iou_distance,
                self.tracks, detections, confirmed_tracks, debugger=self.debugger, size_threshold=self.size_threshold,
                reid_weight=self.reid_weight, tolerance=self.tolerance
            )

            # Associate remaining tracks together with unconfirmed tracks using IOU.
            if self.temporary_metric:
                matches_b, unmatched_tracks_b, unmatched_detections = linear_assignment.min_cost_matching_combined(
                    gated_temporary_metric, iou_matching.iou_cost, self.metric.matching_threshold, self.max_iou_distance,
                    self.tracks, detections, unconfirmed_tracks, unmatched_detections, debugger=self.debugger, size_threshold=self.size_threshold,
                    reid_weight=self.reid_weight, tentative=True, tolerance=self.tolerance
                )
            else:
                matches_b, unmatched_tracks_b, unmatched_detections = linear_assignment.min_cost_matching(
                    iou_matching.iou_cost, self.max_iou_distance, self.tracks,
                    detections, unconfirmed_tracks, unmatched_detections, debugger=self.debugger, 
                )

        matches = matches_a + matches_b
        unmatched_tracks = list(set(unmatched_tracks_a + unmatched_tracks_b))
        return matches, unmatched_tracks, unmatched_detections


    def _initiate_track(self, detection):
        mean, covariance = self.kf.initiate(detection.to_xyah())
        new_feature = detection.feature
        new_track = Track(
            mean,
            covariance,
            self._next_id,
            self.n_init,
            self.max_age,
            detection,
            self.frame_index,
            new_feature
        )
        self.tracks.append(new_track)
        self._next_id += 1

        if self.debugger:
            self.debugger.save_track_info([TrackInfo(new_track.track_id, detection.id, new_feature, new_track.state)])
